const parse = require('csv-parse/lib/sync')
const { join } = require('path')
const { readFile } = require('fs/promises')

/*
  Ce qu'il se passe pour construire la carte sur le site web:

  1. (dans `structures.csv`) le fichier est alimenté avec la liste des structures
  2. (sur `https://adresse.data.gouv.fr/csv`) le fichier est géocodé à partir de la colonne `Adresse`
  3. (ici) la donnée globale Eleventy `structures` est constituée au format JSON/GeoJSON
  4. (dans `api/structures.html`) la donnée globale Eleventy `structures` est mise à disposition via le chemin `/api/structures.json`
  5. (dans `carte.md`) la page carte appelle les données via le chemin `/api/structures.json`
 */

module.exports = async function () {
  const csv = await readFile(join(__dirname, '..', 'structures.csv'))
  const records = parse(csv, {
    columns: true,
    delimiter: ';',
    trim: true,
    skip_empty_lines: true
  })

  const features = records
    .filter(({ Afficher }) => String(Afficher).toLocaleLowerCase() === 'oui')
    .map(record => {
      const coordinates = [
        parseFloat(record.longitude),
        parseFloat(record.latitude)
      ]

      const properties = Object.entries(record).reduce((obj, [key, value]) => {
        if (/^result_/.test(key) === false) {
          obj[key] = value
        }

        return obj
      }, {})

      return {
        type: 'Feature',
        properties,
        geometry: {
          type: 'Point',
          coordinates
        }
      }
    })

  return {
    type: 'FeatureCollection',
    features
  }
}
