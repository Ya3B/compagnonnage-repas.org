---
title: Compagnonnage R.E.P.A.S.
subtitle: Comment participer ?
order: 6
image:
---
# Comment participer au compagnonnage R.E.P.A.S. ?

Télécharge et complète le document suivant : 

[Dossier de candidature](#"candidature").


Tu peux nous les envoyer par mail à l’adresse : contact@compagnonnage-repas.org
Ou par courrier à : Village de Lauconie 19150 CORNIL

Date limite de réception de votre dossier :

## 23 Avril 2022

Pour aider à financer ton parcours et plus largement au compagnonnage, tu peux aller chercher des sous auprès de :
mission locale, pôle emploi, service civique,etc.

Le document suivant t’aidera à parler la langue de bois...
[Document institutions](#"institutions")
