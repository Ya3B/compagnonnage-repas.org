---
title: Le Compagnonnage R.E.P.A.S.
subtitle: Structures Accueillantes
order: 5
image: /img/iconographie/IMG_8851.jpg
mainClass: map
---

## Carte de France des structures

Molette souris pour zoomer ou dézoomer.
Clic 'n drag pour se déplacer.

<!-- la carte est insérée à la suite de ce contenu -->
<!-- elle est contruite à partir du fichier `../structures.csv` -->

<div id="structures-carte"
  data-base-layer-url="{{ '/js/metropole-version-simplifiee.geojson' | url }}"
  data-structures-url="{{ '/api/structures.json' | url }}">
</div>
