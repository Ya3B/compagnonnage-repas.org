---
title: Compagnonnage R.E.P.A.S.
subtitle: Paroles de compagnon‧ne‧s
order: 4
image: /img/iconographie/compaRepas-11.jpg
---
## Mathilde, compagnonne 2014
https://vimeo.com/150779943

## Nelle
Je dirais qu’il m’a permis de confirmer des choix… Je suis sûre aujourd’hui d’avoir envie de vivre l’alternatif au quotidien et ce, grâce aux personnes que le compagnonnage m’a permis de rencontrer… ce sont des gens qui vivent dans la réalité et non dans l’extrême.
## Joaquim
C’est une formation alternative qui m’a servi de tremplin pour partir à la construction de mes rêves, croire que ces derniers peuvent être réalisables et complètement intégrés à la société de manière viable.
## Myriam
Le compagnonnage m’a tant apporté ! D’ailleurs, j’en parle à plein de gens. C’est une autre façon de travailler, de consommer, une autre forme d’apprendre qui m’a le plus marquée et puis, un réseau de connaissances avec lequel je peux échanger.
## Catherine
Ce fut une ouverture d’esprit, une approche concrète de vies différentes, un commencement de réponse à ma vision utopiste.
## Gwen
Le compagnonnage m’a permis de rencontrer, échanger, voir, vivre avec des gens qui avaient fait, chacun à leur façon, un choix de vie. C’est cette expérience d’engagement individuel et collectif que je souhaitais rencontrer. Le fait de cheminer avec d’autres compagnons, avec chacun ses recherches personnelles, permet d’échanger et d’affirmer ses attentes et désirs.
## Lena
C’est une formation à la culture coopérative, un apprentissage concret du comment travailler ensemble et j’ai glané des images de possibles. Le parcours permet de connaître l’existence d’expériences, et ça aide à entrer dans la sienne.
## Vincent
À travers la coopération, on ose davantage prendre des initiatives car ça semble réalisable plus facilement et rapidement, et ça l’est effectivement.
Le compagnonnage m’a permis de rencontrer, échanger, voir, vivre avec des gens qui avaient fait, chacun à leur façon, un choix de vie. C’est cette expérience d’engagement individuel et collectif que je souhaitais rencontrer. Le fait de cheminer avec d’autres compagnons, avec chacun ses recherches personnelles, permet d’échanger et d’affirmer ses attentes et désirs.
