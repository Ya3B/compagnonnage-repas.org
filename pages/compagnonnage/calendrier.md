---
title: Compagnonnage R.E.P.A.S.
subtitle: Déroulement & Calendrier
order: 3
image: /img/iconographie/P1400928.jpg
---
## Réception des candidatures 
jusqu’au dimanche 24 avril 2022 (qu’on ait le temps de les lire !)

### Sélection des candidatures 
le 7 mai 2022


## R1 : 1er Regroupement
du dimanche 2 au dimanche 9 octobre 2022

#### La Rencontre 
Constitution du groupe par la rencontre entre les compagnon.ne.s et le comité de pilotage à travers des temps de chantier, d’échanges et d’animations qui viennent questionner notre rapport au collectif, à la société, au travail. 

## Phase de découverte 
du jeudi 13 octobre au jeudi 22 décembre 2022

### Groupe Action Collectif 
Les compagnon.ne.s vivent et travaillent en collectif en menant un chantier auprès d’une structure accueillante
### ET

### Immersion en Structure
Les compagnon.ne.s s’immergent dans un collectif existant en prenant part aux activités des structures accueillantes.

## R2 : 2ème Regroupement
du samedi 7 au 15 janvier 2023

### Bilan individuel & collectif 
Retours sur les expériences individuelles et collectives menées pendant la phase de découverte. Echanges de Pratiques
Co-construction collective de la la suite des parcours 

## Phase d’expérimentation 
à partir du 16 Janvier 2023



### Groupe Action Collectif 
Les compagnon.ne.s vivent et travaillent en collectif en menant un chantier auprès d’une structure accueillante
### OU
### Immersion en Structure
Les compagnon.ne.s s’immergent dans un collectif existant en prenant part aux activités des structures accueillantes.

## R3: 3ème Regroupement
les 22 et 23 mai 2023
### Bilan de la Pédagogie
Le but de ce temps est de nourrir les futurs compagnonnages
