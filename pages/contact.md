---
title: Contact
---

Pour nous contacter, merci de remplir le formulaire ci-dessous.

<p id="success" hidden>
  Message envoyé.
</p>

<form name="contact" action="{{ '/contact/' | url }}#success" method="POST" data-netlify="true">
  <input type="hidden" name="form-name" value="contact">

  <p>
    <label for="contact-prenom">Prénom</label>
    <input type="text" autocomplete="given-name" name="first_name" id="contact-prenom" required>
  </p>

  <p>
    <label for="contact-nom">Nom</label>
    <input type="text" autocomplete="family-name" name="last_name" id="contact-nom">
  </p>

  <p>
    <label for="contact-email">Email</label>
    <input type="email" autocomplete="email" name="email" id="contact-email" required>
  </p>

  <p>
    <label for="contact-telephone">Téléphone</label>
    <input type="tel" autocomplete="tel" name="telephone" id="contact-telephone">
  </p>

  <p>
    <label for="contact-message">Message</label>
    <textarea id="contact-message" name="message" required></textarea>
  </p>

  <p>
    <button type="submit">Envoyer</button>
  </p>
</form>
