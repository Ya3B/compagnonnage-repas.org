---
title: Conception et crédits
---

## Design

## Développement et méthodologie projet

- **Temps total** : {{ temps.total | inHours }} heures ({{ temps.total | inDays }} jours)
- Temps passé par **Sandra** : {{ temps.sandra | inHours }} heures ({{ temps.sandra | inDays }} jours)
- Temps passé par **Thomas** : {{ temps.thomas | inHours }} heures ({{ temps.thomas | inDays }} jours)

## Logiciels libres

- [Eleventy](https://www.11ty.dev)
- [csv-parse](https://www.npmjs.com/package/csv-parse)
- [MapLibre](https://maplibre.org/)

## Services

- [GitLab](https://gitlab.com/reseau-repas/compagnonnage-repas.org)
- [GitLab CI](https://gitlab.com/reseau-repas/compagnonnage-repas.org/-/pipelines)

## Données ouvertes

- [France GeoJSON](https://github.com/gregoiredavid/france-geojson)
