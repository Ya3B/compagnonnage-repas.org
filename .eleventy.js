const { inDays, inHours } = require('./js/nunjuck-filters.js')

module.exports = function(eleventyConfig) {
    // Output directory: _site

    // Copy `css/` to `_site/css/`
    // Keeps the same directory structure.
    eleventyConfig.addPassthroughCopy("css");

    // Copy `js/` to `_site/js/`
    // Keeps the same directory structure.
    eleventyConfig.addPassthroughCopy("js");

    // Copy `img/` to `_site/img/`
    // Keeps the same directory structure.
  eleventyConfig.addPassthroughCopy("img");
  
  // Copy `pdf/` to `_site/pdf/`
  // Keeps the same directory structure.
  eleventyConfig.addPassthroughCopy("pdf");

    // Crée une "collection", qui regroupe tous les fichiers Markdown situés dans le répertoire `pages/compagnonnage`
    // La collection est utilisable dans les templates Nunjucks sous le nom de `compagnonnage`.
    // @see https://www.11ty.dev/docs/collections/
    eleventyConfig.addCollection("compagnonnage", function(collectionApi) {
      return collectionApi
        .getFilteredByGlob("pages/compagnonnage/*.md")
        .sort((a, b) => a.data.order - b.data.order)
    })

    // https://www.11ty.dev/docs/filters/
    eleventyConfig.addFilter("inHours", inHours)
    eleventyConfig.addFilter("inDays", inDays)

    return {
      dir: {
        input: "pages",
        includes: "../_includes",
        data: "../_data",
      }
    }
  };
