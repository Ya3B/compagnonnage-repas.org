function inHours (minutes) {
  return Math.round(minutes / 60)
}

function inDays (minutes) {
  return Math.round(minutes / 60 / 6)
}

module.exports = { inHours, inDays }
