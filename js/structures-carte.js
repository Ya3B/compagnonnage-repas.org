function mapSetup (mapContainer) {
  const { baseLayerUrl, structuresUrl } = mapContainer.dataset
  const { LngLatBounds } = maplibregl

  // Initialise la carte MapLibre avec des options
  const map = new maplibregl.Map({
    // element HTML dans lequel la carte sera construite
    container: mapContainer,
    // niveau de zoom initial (1 = terre entière, 22 = ras du sol)
    zoom: 3,
    // niveau de zoom jusqu'où on peut reculer
    minZoom: 0,
    // niveau de zoom jusqu'où on peut plonger
    maxZoom: 7,
    // coordonnées GPS du centre de la carte (plus ou moins le milieu de la France métropolitaine)
    center: [2.209667, 46.232193],
    style: {
      id: "compagnonnage-repas",
      version: 8,
      sources: {
        // ajoute le découpage de la France métropolitaine comme source de données
        // c'est le fichier `js/metropole-version-simplifiee….geojson
        france: {
          type: "geojson",
          data: baseLayerUrl
        },
        // ajoute l'emplacement des structures REPAS comme source de données
        // c'est le fichier `api/structures.json` construit par `_data/structures.js` et publiées par `pages/api/structures.html`
        structures: {
          type: "geojson",
          data: structuresUrl
        }
      },
      layers: [
        {
          // crée un calque d'affichage basé sur le découpage de la France
          // on remplit la forme vectorielle avec un couleur
          id: 'country-boundaries',
          type: 'fill',
          source: 'france',
          paint: {
            'fill-color': '#E5E5E5',
          }
        },
        {
          // crée un calque d'affichage des structures sous forme de marqueurs
          id: 'structures-points',
          type: 'symbol',
          source: 'structures',
          layout: {
            'icon-image': 'marker',
            'icon-size': 0.75
          }
        }
      ]
    },
    // carré de coordonnées GPS dans lequel on peut se déplacer
    // si on ne les précise pas, on peut aller se balader dans d'autres pays
    maxBounds: new LngLatBounds(
      [-17.391908446253353, 41.138099752838855],
      [24.376254930457293, 51.14611670016029],
    ),
    // quand on déplace la carte, actualise le "hash" avec les coordonnées du centre de la carte
    // exemple : #/-2,46,3.5
    hash: true,
    // langue des éléments textuels de la carte, s'il y en a
    locale: 'fr'
  })

  // on définit l'image marqueur à partir d'un fichier
  // le code est obtenu en glissant une image sur https://www.base64-image.de/
  const image = new Image(34, 47)
  image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAvCAYAAACG2RgcAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAALlSURBVHgBzZgxTBNRGMf/37vDocTFEmA4Y1xw1MEFtgoyqSOrTu7GSYwJRsDEhLhB4ubKaLtIwFEWh7KyaGIHxJbxxLT3Pt93Uiht73rX9u76S9rh7tL79Xvf9973HjAiUJyHn376lsvnpm+T1reg9HWL7GuaOSf3FJHrceOEya7ajHIDOHy74NSi/nYkkRe7lRmCfkigGcRAE5XB2DNCh72eDRWRCEyMTy4R0ywGgIn3mVUxLEKBIhIFC/oJg/IYAgSuae1try/eLHe/34WXn3/MwbIeIwlIl9bmbxTbL6v2C8s73+8kJiGwerC8U5lHmIgZjrxljSUncQYpXpKhR5CIyYnnzXJMGsk/KYYOkVe7ldlhJWYU5F2TV6fvdYiwmSeQMqwx34yKL5J2NC5MONeMii/iaT2HjNCeWS4MtGJCU89NvUeG/HZ/PVN/c9MOMmbKLKTKrKCZizCQV+BGKvNGGFrrCYURYYREyHaRMUqpqtLenxNkTF2N/VRsvpAxTPWakvZNuidkhWm63xWcip+sxKqMjNAafmPtizQImYng7N2+iLT7si9B2ph31tyjg3MRgc3+AynDnj748Oiue0nEdo++pB0VVuq8mz8XWTFmmtPLFda837rhujTFa6CUVlRao9EhIoZp5Ep7NDpEhKRzhc3k2R6NriKSK55X/4ik0Gqv22a8axsgG2UTvp5HCXFheJX1Rafr0Af2I1dOj7eGOkTmtxj2ZtDtQBEZogZjC0OCPZTCzkdCOzR/6mcauIqkSoKGJJKI8Oa+s22W5757FqmS6unxdq/nIvWsmtUW99Gz+KUKtdFcTwYWkbEds8Y3YyevOaqKerIYuYt/XchXTPL2DHETsqgYdF42kIhg/t2+OeIs9npOJFYLTgkxiL2vWV1wSqGVZNHXuBJ9iQh+JZkXtl+XmXOt4PS1PPS905MXti4DIlF1qxvok4G2nLIMyBzzv0ztzShlmhhyJCofDMg/i3BCtLV6ltsAAAAASUVORK5CYII='
  map.addImage('marker', image);

  // création d'un objet "popup"
  // on fait des choses avec ce popup lors d'interactions sur la carte
  // @see https://maplibre.org/maplibre-gl-js-docs/api/markers/#popup
  const popup = new maplibregl.Popup({
    closeButton: true,
    closeOnClick: true,
    offset: {
      'top': [0, 0],
      'bottom': [0, 0]
    }
  })

  // quand on clique sur un point du calque `structures-points`, on affiche la popup
  map.on('click', 'structures-points', ({ features }) => {
    const [point] = features
    const { coordinates } = point.geometry
    const { Nom, Adresse, SiteWeb } = point.properties

    const html = `
      <h3>${Nom}</h3>
      <p>${Adresse}</p>
      ${SiteWeb && `<p>
        <a href="${SiteWeb}" target="_blank" rel="noopener noreferrer">${SiteWeb}</a>
      </p>`}
    `

    popup.setLngLat(coordinates).setHTML(html).addTo(map)
  })

  // on change le curseur de la souris lorsqu'on survole un marquer de structure
  map.on('mouseenter', 'structures-points', function () {
    map.getCanvas().style.cursor = 'pointer'
  })
  map.on('mouseleave', 'structures-points', function () {
    map.getCanvas().style.cursor = ''
  })
}


/**
 * Setup the map on page load, if we can see a `<div id="structures-carte">` element
 */
document.addEventListener('DOMContentLoaded', () => {
  const mapContainer = document.querySelector('#structures-carte')

  if (mapContainer) {
    mapSetup(mapContainer)
  }
})
